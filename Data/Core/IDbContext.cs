﻿using System.Linq;

namespace Data.Core
{
    using Entities;

    public interface IDbContext
    {
        IQueryable<PersonEntity> People { get; set; }
        IQueryable<CarEntity> Cars { get; set; }
    }
}
