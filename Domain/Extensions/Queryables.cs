﻿using System.Linq;

namespace Domain.Extensions
{
    using DTO;
    public static class Queryables
    {
        public static IQueryable<Person> FilterByLastName(this IQueryable<Person> people, string name) =>
            people.Where(p => p.LastName == name.ToUpper());
    }
}
