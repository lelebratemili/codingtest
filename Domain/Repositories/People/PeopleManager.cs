﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Repositories.People
{
    using Data.Core;
    using Data.Entities;
    using DTO;
    using AutoMapper;

    public class PeopleManager : IPeopleManager
    {
        private readonly IDbContext _db;
        private readonly IMapper _mapper;

        public PeopleManager() : this(new FakeDatabase())
        {
        }

        public PeopleManager(IDbContext db)
        {
            _db = db;

            var config = new MapperConfiguration(cfg => {
                
                cfg.CreateMap<PersonEntity, Person>()
                    .ForMember(dest => dest.State, src => src.MapFrom(s => Enum.Parse(typeof(State), s.State)))
                    .ForMember(dest => dest.Gender, src => src.MapFrom(s => Enum.Parse(typeof(Gender), s.Gender)))
                    .ReverseMap()
                    .ForMember(dest => dest.State, src => src.MapFrom(s => s.State.ToString()))
                    .ForMember(dest => dest.Gender, src => src.MapFrom(s => s.Gender.ToString()));
            });

            _mapper = config.CreateMapper();
        }

        public async Task<IQueryable<Person>> GetPeople() => await Task.FromResult(_db.People.Select(_mapper.Map<Person>).AsQueryable());
    }
}
