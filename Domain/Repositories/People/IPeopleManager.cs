﻿using System.Linq;
using System.Threading.Tasks;

namespace Domain.Repositories.People
{
    using DTO;
    public interface IPeopleManager
    {
        Task<IQueryable<Person>> GetPeople();
    }
}
