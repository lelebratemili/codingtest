﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace CodingTest
{
    using Domain.Extensions;
    using Domain.Repositories.People;

    class Program
    {
        static void Main(string[] args)
        {
            DoWork().GetAwaiter().GetResult();
        }

        static async Task DoWork()
        {
            var peopleManager = new PeopleManager();
            var people = await peopleManager.GetPeople();

            var numberOfPeople = people.FilterByLastName("PURNELL").Count();

            Console.WriteLine($"There are {numberOfPeople} in the people database.");

            Console.ReadKey();
        }
    }
}
